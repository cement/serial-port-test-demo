package com.aill.serialportdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.aill.androidserialport.SerialPort;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidParameterException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SerialPort.setSuPath("/system/xbin/su");
        openPort();
    }

    private SerialPort mSerialPort;
    private FileOutputStream mOutputStream;
    private FileInputStream mInputStream;

    /***
     * ttyS1 7.0Android 改成ttyS4
     */
    private void openPort() {
        WkLogTool.showLog("打开 串口 ");
        try {
            mSerialPort = new SerialPort(new File("/dev/ttyS1"), 115200, 0);
            mOutputStream = (FileOutputStream) mSerialPort.getOutputStream();
            mInputStream = (FileInputStream) mSerialPort.getInputStream();
        } catch (SecurityException e) {
            WkLogTool.showLogError("You do not have read/write permission to the serial  port");
        } catch (IOException e) {
            WkLogTool.showLogError("The serial port can not be opened for an unknown  reason");
        } catch (InvalidParameterException e) {
            WkLogTool.showLogError("Please configure your serial port first");
        }


        WkLogTool.showLog("串口打开情况 mSerialPort= " + mSerialPort);
        WkLogTool.showLog("串口打开情况 mInputStream= " + mInputStream);
        WkLogTool.showLog("串口打开情况 mOutputStream= " + mOutputStream);

        startSendDataToPort();
        startReceivePortData(mInputStream);
    }

    private void startSendDataToPort() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (isRuning) {
                    try {
                        if (mSerialPort == null || mOutputStream == null) {
                            WkLogTool.showLogError("数据发送异常 串口 SerialPort 或 mOutputStream  == null");
                        } else {
                            byte[] data = {(byte) 0xdd, (byte) 0xdd,0x00,(byte) 0xdd, (byte) 0xdd,0x00,(byte) 0xdd, (byte) 0xdd,0x00};
                            if (data != null) {
                                mOutputStream.write(data);
                                WkLogTool.showLogError("数据发送成功 " + DataConversionUtils.byte2HexStr(data));
                            }
                        }
                    } catch (Exception e) {
                        WkLogTool.showLogError("数据发送异常 " + e.getMessage());
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private boolean isRuning = true;

    private void startReceivePortData(final FileInputStream mInputStream) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                WkLogTool.showLog("接收串口数据的 线程启动");
                byte[] buffer = new byte[512];
                int size = 0;
                while (isRuning) {
                    try {
                        if (mInputStream != null) {
                            // 这个方法 会阻塞， 如果一直读取不到串口数据。
                            size = mInputStream.read(buffer);
                            if (size > 0) {
                                // 截取有效数据
                                byte[] realBytes = DataConversionUtils.subBytes(buffer, 0, size);
                                String s = DataConversionUtils.byte2HexStr(realBytes);
                                WkLogTool.showLog("接收数据成功 " + s);
                            } else {
                                WkLogTool.showLogError("接收串口数据异常  size=" + size);
                            }
                        } else {
                            Thread.sleep(100);
                            WkLogTool.showLogError("串口 mInputStream ==null");
                        }
                    } catch (Exception e) {
                        WkLogTool.showLogError("串口 异常111  == " + e.getMessage());
                        e.printStackTrace();
                    }
                }

            }
        }).start();
    }

}
