package com.aill.serialportdemo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DataConversionUtils {

    /**
     * bytes转换成十六进制字符串
     *
     * @param b byte数组
     * @return String 每个Byte值之间空格分隔
     */
    public static String byte2HexStr(byte[] b) {
        String stmp;
        StringBuilder sb = new StringBuilder("");
        for (int n = 0; n < b.length; n++) {
            stmp = Integer.toHexString(b[n] & 0xFF);
            sb.append((stmp.length() == 1) ? "0" + stmp : stmp);
            sb.append(" ");
        }
        return sb.toString().toUpperCase().trim();
    }




    /***
     * int 类型 转为 1个字节长度的 byte
     */
    public static byte intTo1ByteHex(int intVaue) {
        return new Integer(intVaue & 0xff).byteValue();
    }

    /***
     * int 类型 转为 1个字节长度的 byte数组、
     * ？？
     */
    public static byte[] intTo1ByteHexArray(int intVaue) {
        byte[] array = new byte[1];
        array[0] = new Integer(intVaue & 0xff).byteValue();
        return array;

    }

    /***
     * int 类型 转为 4个字节长度的 byte数组。
     * 大端小端翻转 。
     * 邹聪提供。
     * @param intVaue  int
     * @param byteVauesHex byte[]
     */
    public static void intTo4ByteArrayHex(int intVaue, byte[] byteVauesHex) {
        for (int i = 0; i < byteVauesHex.length; i++) {
            byteVauesHex[i] = new Integer(intVaue & 0xff).byteValue();
            intVaue = intVaue >> 8;
        }
    }




    /**
     * 两个byte转整形，add zc
     *
     * @param res byte[]
     * @return int
     */
    public static int byte2int(byte[] res) {
        // 一个byte数据左移24位变成0x??000000，再右移8位变成0x00??0000
        return (res[0] & 0xff) | ((res[1] << 8) & 0xff00);
    }


    /***
     * int 类型 转为 2个字节长度的 byte数组。
     * 大端小端翻转 。
     * 邹聪提供。
     * @param intVaue  int
     * @param byteVauesHex byte[]
     */
    public static void intTo2ByteArrayHex(int intVaue, byte[] byteVauesHex) {
        for (int i = 0; i < byteVauesHex.length; i++) {
            byteVauesHex[i] = new Integer(intVaue & 0xff).byteValue();
            intVaue = intVaue >> 8;
        }
    }


    /***
     * int 类型 转为 2个字节长度的 byte数组。
     * 正常顺序。
     * @param intVaue  int
     */
    public static byte[] intTo2ByteArrayHexNormal(int intVaue) {
        byte[] byteVauesHex = new byte[2];
        for (int i = byteVauesHex.length - 1; i >= 0; i--) {
            byteVauesHex[i] = new Integer(intVaue & 0xff).byteValue();
            intVaue = intVaue >> 8;
        }
        return byteVauesHex;
    }

    /**
     * 将4个字节长度的 byte数组转为int。
     * 本方法适用于(低位在后，高位在前)的顺序
     */
    public static int bytes4HeightAndLowToInt(byte[] src, int offset) {
        return (((src[offset] & 0xFF) << 24)
                | ((src[offset + 1] & 0xFF) << 16)
                | ((src[offset + 2] & 0xFF) << 8)
                | (src[offset + 3] & 0xFF));
    }


    /**
     * 将4个字节长度的 byte数组转为int。<br>
     * 本方法适用于(低位在前，高位在后)的顺序<br>
     * 320项目中用到
     */
    public static int bytes4LowAndHeightToInt(byte[] src, int offset) {

        return ((src[offset] & 0xFF)
                | ((src[offset + 1] & 0xFF) << 8)
                | ((src[offset + 2] & 0xFF) << 16)
                | ((src[offset + 3] & 0xFF) << 24));
    }

    /**
     * 将1个字节长度的 byte数组转为int。
     */
    public static int bytes1ToInt(byte[] src) {
        return src[0] & 0xFF;
    }

    /**
     * 将一个字节长度的byte 转为 int
     *
     * @param src byte
     * @return int
     */
    public static int byte1ToInt(byte src) {
        return src & 0xFF;
    }



    /**
     * 数组截取
     *
     * @param srcByte 源数组
     * @param begin   源数组要复制的起始位置
     * @param count   要复制的长度
     * @return
     */
    public static byte[] subBytes(byte[] srcByte, int begin, int count) {
        byte[] result = new byte[count];
        System.arraycopy(srcByte, begin, result, 0, count);
        return result;
    }

    /**
     * 查找列表中与目标值的绝对值最小的 数
     *
     * @param list   列表
     * @param tarNum 目标
     * @return 符合条件的数字在数组中的下标
     */
    public static int getNearestValueIndex(ArrayList<Integer> list, int tarNum) {
        //假设第一个最小
        int result = Math.abs(list.get(0) - tarNum);
        int index = 0;

        for (int i = 0; i < list.size(); i++) {
            int abVlue = Math.abs(list.get(i) - tarNum);
            if (result > abVlue) {
                result = abVlue;
                //将较小的保存
                index = i;
            }
        }
        return index;
    }


    /**
     * 公制转英制
     *
     * @return float
     */
    public static float metricToFeet(float src) {
        return (float) (src / 0.3048);
    }









    /**
     * 获取格式化的时间格式
     *
     * @return 2017_05_31_16_23_31
     */
    public static String getCurrentDataStr() {
        //常用的日期格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
        //日期类型--->String
        String dateStr = sdf.format(new Date());
        return dateStr;
    }

    /**
     * 获取格式化的时间格式
     *
     * @return 2017-07-07 10:24
     */
    public static String getCurrentDataStrWithYear_day_hore_min() {
        //常用的日期格式
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //日期类型--->String
        String dateStr = sdf.format(new Date());
        return dateStr;
    }






    /**
     * 将字符串中的 逗号 转为 小数点
     *
     * @return String
     */
    public static String partDou2Point(String sorceStr) {
        sorceStr = sorceStr.replaceAll(",", ".");
        return sorceStr;
    }




}
