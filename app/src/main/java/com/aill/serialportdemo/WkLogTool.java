package com.aill.serialportdemo;

import android.util.Log;


public class WkLogTool {
    private static boolean isDebug = true;

    public static void showLog(String msg) {
        if (isDebug) {
            Log.i("数据测试", msg);
        }
    }

    public static void showLogError(String msg) {
        if (isDebug) {
            Log.e("数据测试", msg);
        }
    }
}
